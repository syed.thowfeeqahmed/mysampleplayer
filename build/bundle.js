'use strict';

class stateManager {
  constructor(){
    this.currentPlayer = undefined;
  }
  getCurrentState(){
    return this.currentPlayer
  }

  // "content" or "Ad"
  setCurrentState(state) {
    console.log("CurrentState ->",state);
    this.currentPlayer = state;
  }
}var stateManager$1 = new stateManager;

class Ads {
constructor(params) {
  console.log("constructor is called");
}
init(a) {
  console.log(a);
  this.getElements();
  this.createAdDisplaycontainer();
  // this.requestADs()
}

getElements() {
this.videoContent = document.getElementById('contentElement');
this.playButton = document.getElementById('playButton');
// var playButton = document.getElementById('playButton');
playButton.addEventListener('click', this.requestADs.bind(this));
}

requestAdplay() {
  console.log("Ad play triggered");
  this.requestADs();
}

// Create an ad display container
createAdDisplaycontainer() {
  this.adDisplayContainer =
    new google.ima.AdDisplayContainer(
        document.getElementById('adContainer'),
        this.videoContent);
// Must be done as the result of a user action on mobile
this.adDisplayContainer.initialize();
 }



// Request ads
requestADs() {
// Re-use this AdsLoader instance for the entire lifecycle of your page.
const adsLoader = new google.ima.AdsLoader(this.adDisplayContainer);

// Add event listeners
adsLoader.addEventListener(
    google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED,
    this.onadsManagerLoaded.bind(this),
    false);
adsLoader.addEventListener(
    google.ima.AdErrorEvent.Type.AD_ERROR,
    this.onAdError.bind(this),
    false);

// An event listener to tell the SDK that our content video
// is completed so the SDK can play any post-roll ads.
this.contentEndedListener = () => {adsLoader.contentComplete();};
this.videoContent.onended = this.contentEndedListener;

// Request video ads.
const adsRequest = new google.ima.AdsRequest();
// adsRequest.adTagUrl = 'https://pubads.g.doubleclick.net/gampad/ads?' +
//     'sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&' +
//     'impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&' +
//     'cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator=';

//pre-roll add
// more example urls at: https://developers.google.com/interactive-media-ads/docs/sdks/html5/tags
adsRequest.adTagUrl = 'https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dskippablelinear&correlator=';
// adsRequest.adTagUrl = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator=";

// Specify the linear and nonlinear slot sizes. This helps the SDK to
// select the correct creative if multiple are returned.
adsRequest.linearAdSlotWidth = 640;
adsRequest.linearAdSlotHeight = 400;
adsRequest.nonLinearAdSlotWidth = 640;
adsRequest.nonLinearAdSlotHeight = 150;
 
  adsLoader.requestAds(adsRequest);
}

onAdError(adErrorEvent) {
  // Handle the error logging and destroy the this.adsManager
  this.adsManager.destroy();
  console.log(adErrorEvent.getError());
}

 onadsManagerLoaded(adsManagerLoadedEvent) {
   console.log("adsmanagerloaded");
  // Get the ads manager.
  this.adsManager = adsManagerLoadedEvent.getAdsManager(
      this.videoContent);  // See API reference for contentPlayback

  // Add listeners to the required events.
  this.adsManager.addEventListener(
      google.ima.AdErrorEvent.Type.AD_ERROR,
      this.onAdError.bind(this));
  this.adsManager.addEventListener(
      google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED,
      this.onContentPauseRequested.bind(this));
  this.adsManager.addEventListener(
      google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED,
      this.onContentResumeRequested.bind(this));

  try {
    // Initialize the ads manager. Ad rules playlist will start at this time.
    this.adsManager.init(640, 360, google.ima.ViewMode.NORMAL);
    // Call start to show ads. Single video and overlay ads will
    // start at this time; this call will be ignored for ad rules, as ad rules
    // ads start when the this.adsManager is initialized.
    console.log("starting the ad");
    this.adsManager.start();
  } catch (adError) {
    // An error may be thrown if there was a problem with the VAST response.
    // Play content here, because we won't be getting an ad.
    this.videoContent.play();
    console.log("ad error occured",adError);
  }
}

 onContentPauseRequested() {
  // This function is where you should setup UI for showing ads (e.g.
  // display ad timer countdown, disable seeking, etc.)
  this.videoContent.removeEventListener('ended', this.contentEndedListener.bind(this));
  this.videoContent.pause();
  stateManager$1.setCurrentState("Ad");
}

 onContentResumeRequested() {
  // This function is where you should ensure that your UI is ready
  // to play content.
  this.videoContent.addEventListener('ended', this.contentEndedListener.bind(this));
  this.videoContent.play();
  stateManager$1.setCurrentState("content");
}
} var Ads$1 = new Ads;

class VideoElementPlayer {
  constructor() {
    // this.init()
    this.adManager = Ads$1;
  }

  init(object) {
    console.log(" video element player is intialized");
    this.videoElement = object.videoplayerId;
    this.defineButtons().then(() => {
      this.addEventListeners();
    });
    // scrolling Console div after set time
    window.setInterval(() => {
      var elem = document.getElementById("console");
      elem.scrollTop = elem.scrollHeight;
    }, 900);
  }

  defineButtons() {
    return new Promise((resolve) => {
      this.videoContent = document.getElementById(this.videoElement);
      this.playButton = document.getElementById("playButton");
      this.pauseButton = document.getElementById("pauseButton");
      this.playAd = document.getElementById("playAd");
      // this.disableControlsButton = document.getElementById("disableControls")
      // this.enableControlsButton = document.getElementById("enableControls")
      resolve();
    })
  }

  addEventListeners() {
    // Sent when playback is aborted; for example, if the media is playing and is restarted from the beginning, this event is sent.
    this.videoContent.addEventListener("abort", this.onAbort.bind(this));
    // The time indicated by the element's currentTime attribute has changed.
    this.videoContent.addEventListener("timeupdate", this.onTimeUpdate.bind(this));
    // Fired when enough data is available that the browser believes it can play the video completely without interruption
    this.videoContent.addEventListener("canplaythrough", this.onCanPlayThrough.bind(this));
    // Fired when the video has finished playing
    this.videoContent.addEventListener("ended", this.onEnded.bind(this));
    // Fired if an error occurs
    this.videoContent.addEventListener("error", this.onError.bind(this));
    // Sent when the media has enough data to start playing, after the play event, but also when recovering from being stalled,
    // when looping media restarts, and after seeked, if it was playing before seeking.
    this.videoContent.addEventListener("playing", this.onPlaying.bind(this));
    // Sent when enough data is available that the media can be played, at least for a couple of frames. This corresponds to the HAVE_FUTURE_DATA readyState.
    this.videoContent.addEventListener("canplay", this.onCanPlay.bind(this));
    // The metadata has loaded or changed, indicating a change in duration of the media.  This is sent, for example, when the media has loaded enough that the duration is known.
    this.videoContent.addEventListener("durationchange", this.onDurationChange.bind(this));
    // The media has become empty; for example, this event is sent if the media has already been loaded (or partially loaded), and the load() method is called to reload it.
    this.videoContent.addEventListener("emptied", this.onEmptied.bind(this));
    // The first frame of the media has finished loading.
    this.videoContent.addEventListener("loadeddata", this.onLoadedData.bind(this));
    // The media's metadata has finished loading; all attributes now contain as much useful information as they're going to
    this.videoContent.addEventListener("loadedmetadata", this.onLoadedMetaData.bind(this));
    // Sent when loading of the media begins.
    this.videoContent.addEventListener("loadstart", this.onLoadStart.bind(this));
    // Sent when an audio buffer is provided to the audio layer for processing; the buffer contains raw audio samples that may or may not already have been played by the time you receive the event
    this.videoContent.addEventListener("mozaudioavailable", this.onMozAudioAvailable.bind(this));
    // Sent when the playback state is changed to paused (paused property is true).
    this.videoContent.addEventListener("pause", this.onPause.bind(this));
    // Sent when the playback state is no longer paused, as a result of the play method, or the autoplay attribute.
    this.videoContent.addEventListener("play", this.onPlay.bind(this));
    // Sent when the playback speed changes
    this.videoContent.addEventListener("ratechange", this.onRateChange.bind(this));
    // Sent when a seek operation completes.
    this.videoContent.addEventListener("seeked", this.onSeeked.bind(this));
    //Sent when a seek operation begins
    this.videoContent.addEventListener("seeking", this.onSeeking.bind(this));
    // Sent when the user agent is trying to fetch media data, but data is unexpectedly not forthcoming.
    this.videoContent.addEventListener("stalled", this.onStalled.bind(this));
    // Sent when loading of the media is suspended; this may happen either because the download has completed or because it has been paused for any other reason.
    this.videoContent.addEventListener("suspend", this.onSuspend.bind(this));
    // Sent when the audio volume changes (both when the volume is set and when the muted attribute is changed).
    this.videoContent.addEventListener("volumechange", this.onVolumeChange.bind(this));
    // Sent when the requested operation (such as playback) is delayed pending the completion of another operation (such as a seek).
    this.videoContent.addEventListener("waiting", this.onWaiting.bind(this));

    // click for all the Buttons
    this.playButton.addEventListener("click", () => {
      const currentState = stateManager$1.getCurrentState();
      if(currentState === "Ad"){
        console.log("currecnt state is :" ,currentState);
       // when playing ad no need to do anything
      }else{
        stateManager$1.setCurrentState("content");
        this.videoContent.play();
      }
    });
    this.pauseButton.addEventListener("click", () => {
      this.videoContent.pause();
    });
    if(this.playAd){
      this.playAd.addEventListener("click", ()=>{
        this.adManager.requestAdplay();
      });
    }
    // this.enableControlsButton.addEventListener("click", this.enableControls.bind(this))
    // this.disableControlsButton.addEventListener("click", this.disableControls.bind(this))
  }

  onSeeked() {
    console.log("onSeeked");
    document.getElementById("console").innerHTML += "<br>message:" + "onSeeked";
  }

  onStalled() {
    console.log("onStalled");
    document.getElementById("console").innerHTML += "<br>message:" + "onStalled";
  }

  onSeeking() {
    console.log("onSeeking");
    document.getElementById("console").innerHTML += "<br>message:" + "onSeeking";
  }

  onSuspend() {
    console.log("onSuspend");
    document.getElementById("console").innerHTML += "<br>message:" + "onSuspend";
  }

  onVolumeChange() {
    console.log("onVolumeChange ", this.videoContent.volume);
    document.getElementById("console").innerHTML += "<br>message:" + "onVolumeChange";
  }

  onWaiting() {
    console.log("onWaiting");
    document.getElementById("console").innerHTML += "<br>message:" + "onWaiting";
  }

  onSeeked() {
    console.log("onSeeked");
    document.getElementById("console").innerHTML += "<br>message:" + "onSeeked";
  }

  onPlay() {
    const src = this.videoContent.currentSrc;
    console.log("onPLay ", src);
    document.getElementById("console").innerHTML += "<br>message:" + "onPlay scr = " + src;
  }

  onRateChange() {
    console.log("onRateChange");
    document.getElementById("console").innerHTML += "<br>message:" + "onRateChange";
  }

  onPause() {
    console.log("onPause");
    document.getElementById("console").innerHTML += "<br>message:" + "onPause";
  }

  onMozAudioAvailable() {
    console.log("onMozAudioAvailable");
    document.getElementById("console").innerHTML += "<br>message:" + "onMozAudioAvailable";
  }

  onLoadStart() {
    console.log("onLoadStart");
    document.getElementById("console").innerHTML += "<br>message:" + "onLoadStart";
  }
  onLoadedMetaData() {
    console.log("onLoadedMetaData");
    document.getElementById("console").innerHTML += "<br>message:" + "onLoadedMetaData";
  }

  onLoadedData() {
    console.log("onloadedData");
    document.getElementById("console").innerHTML += "<br>message:" + "onLoadedData";
  }
  onEmptied() {
    console.log("On emptied");
    document.getElementById("console").innerHTML += "<br>message:" + "onEmptied";
  }

  onDurationChange() {
    console.log("Duration change");
    document.getElementById("console").innerHTML += "<br>message:" + "onDurationChange";
  }

  onCanPlay() {
    console.log("onCanplay-HAVE FUTUREDATA READY STATE");
    document.getElementById("console").innerHTML += "<br>message:" + "onCanPlay";
  }

  onAbort() {
    console.log("onAbort");
    document.getElementById("console").innerHTML += "<br>message:" + "onAbort";
  }

  onPlaying() {
    console.log("onPlaying");
    document.getElementById("console").innerHTML += "<br>message:" + "onPlaying";
  }

  onError() {
    console.log("error occured");
    document.getElementById("console").innerHTML += "<br>message:" + "onError";
  }

  onTimeUpdate() {
    let currentTime = this.videoContent ? this.videoContent.currentTime : undefined;
    console.log("TimeUpdate ", currentTime);
    document.getElementById("console").innerHTML += "<br>message:" + "onTimeUpdate " + currentTime;
  }

  onCanPlayThrough() {
    console.log("onCanPlayThrough");
    document.getElementById("console").innerHTML += "<br>message:" + "onCanPlayThrough";
  }
  onEnded() {
    console.log("onEnded");
    document.getElementById("console").innerHTML += "<br>message:" + "onEnded";
  }

  enableControls() {
    this.videoContent.controls = true;
    this.videoContent.load();
    console.log("enableControls");
    document.getElementById("console").innerHTML += "<br>message:" + "enableControls";
  }

  disableControls() {
    this.videoContent.controls = false;
    this.videoContent.load();
    console.log("disableControls");
    document.getElementById("console").innerHTML += "<br>message:" + "disableControls";
  }

  checkControls() {
    document.getElementById("console").innerHTML += "<br>message:" + "checkControls" + this.videoContent.controls;
    return this.videoContent.controls
  }

  //  addListenerMulti(el, s, fn) {
  //   s.split(' ').forEach(e => el.addEventListener(e, fn, false));
  // }

  // var video = document.getElementById('#video');

  // addListenerMulti(video, 'abort canplay canplaythrough durationchange emptied encrypted  ended error interruptbegin interruptend loadeddata loadedmetadata loadstart mozaudioavailable pause play playing progress ratechange seeked seeking stalled suspend timeupdate volumechange waiting', (e){
  //     console.log(e.type);
  // });
}
var VideoElementPlayer$1 = new VideoElementPlayer();

const videoplayerId="contentElement";const player="VideoElementPlayer";const play_ADS=true;const ADPlayerID="adContainer";var playerConfig = {videoplayerId:videoplayerId,player:player,play_ADS:play_ADS,ADPlayerID:ADPlayerID};

// import SpatialNavigation from "spatial-navigation-js";
class buttonNavigation {
  initilize() {
    SpatialNavigation.init();
    SpatialNavigation.add({
      selector: '.focusable'
    });
    SpatialNavigation.makeFocusable();
    SpatialNavigation.focus(".focusable");
  }
}
var ButtonNavigation = new buttonNavigation;

if(playerConfig.player === "VideoElementPlayer") {
  VideoElementPlayer$1.init(playerConfig);
}

if(playerConfig.play_ADS) {
  Ads$1.init();
}
ButtonNavigation.initilize();
