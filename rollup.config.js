import json from 'rollup-plugin-json';

export default {
  input: './src/js/main.js',
  output: {
    file: 'build/bundle.js',
    format: 'cjs'
  },

  plugins: [
    json({ 
      // for tree-shaking, properties will be declared as
      // variables, using either `var` or `const`
      preferConst: true, // Default: false
 
      // specify indentation for the generated default export —
      // defaults to '\t'
      indent: '  ',
 
      // ignores indent and generates the smallest code
      compact: true, // Default: false
 
      // generate a named export for every property of the JSON object
      namedExports: true // Default: true
    })
  ]
  
};