import VideoElementPlayer from "./utility";
import Ads from "./ads";
import stateManager from "./stateManager";
import playerConfig from './playerConfig.json';
import ButtonNavigation from "./spatial_nav";


if(playerConfig.player === "VideoElementPlayer") {
  VideoElementPlayer.init(playerConfig);
}

if(playerConfig.play_ADS) {
  Ads.init();
}
ButtonNavigation.initilize();
