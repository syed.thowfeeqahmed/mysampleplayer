// import SpatialNavigation from "spatial-navigation-js";
class buttonNavigation {
  initilize() {
    SpatialNavigation.init();
    SpatialNavigation.add({
      selector: '.focusable'
    });
    SpatialNavigation.makeFocusable();
    SpatialNavigation.focus(".focusable");
  }
}
export default new buttonNavigation;