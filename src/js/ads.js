import stateManager from "./stateManager"
class Ads {
constructor(params) {
  console.log("constructor is called")
}
init(a) {
  console.log(a)
  this.getElements()
  this.createAdDisplaycontainer()
  // this.requestADs()
}

getElements() {
this.videoContent = document.getElementById('contentElement');
this.playButton = document.getElementById('playButton');
// var playButton = document.getElementById('playButton');
playButton.addEventListener('click', this.requestADs.bind(this));
}

requestAdplay() {
  console.log("Ad play triggered")
  this.requestADs()
}

// Create an ad display container
createAdDisplaycontainer() {
  this.adDisplayContainer =
    new google.ima.AdDisplayContainer(
        document.getElementById('adContainer'),
        this.videoContent);
// Must be done as the result of a user action on mobile
this.adDisplayContainer.initialize();
 }



// Request ads
requestADs() {
// Re-use this AdsLoader instance for the entire lifecycle of your page.
const adsLoader = new google.ima.AdsLoader(this.adDisplayContainer);

// Add event listeners
adsLoader.addEventListener(
    google.ima.AdsManagerLoadedEvent.Type.ADS_MANAGER_LOADED,
    this.onadsManagerLoaded.bind(this),
    false);
adsLoader.addEventListener(
    google.ima.AdErrorEvent.Type.AD_ERROR,
    this.onAdError.bind(this),
    false);

// An event listener to tell the SDK that our content video
// is completed so the SDK can play any post-roll ads.
this.contentEndedListener = () => {adsLoader.contentComplete();};
this.videoContent.onended = this.contentEndedListener;

// Request video ads.
const adsRequest = new google.ima.AdsRequest();
// adsRequest.adTagUrl = 'https://pubads.g.doubleclick.net/gampad/ads?' +
//     'sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&' +
//     'impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&' +
//     'cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator=';

//pre-roll add
// more example urls at: https://developers.google.com/interactive-media-ads/docs/sdks/html5/tags
adsRequest.adTagUrl = 'https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dskippablelinear&correlator=';
// adsRequest.adTagUrl = "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator=";

// Specify the linear and nonlinear slot sizes. This helps the SDK to
// select the correct creative if multiple are returned.
adsRequest.linearAdSlotWidth = 640;
adsRequest.linearAdSlotHeight = 400;
adsRequest.nonLinearAdSlotWidth = 640;
adsRequest.nonLinearAdSlotHeight = 150;
 
  adsLoader.requestAds(adsRequest);
}

onAdError(adErrorEvent) {
  // Handle the error logging and destroy the this.adsManager
  this.adsManager.destroy();
  console.log(adErrorEvent.getError());
}

 onadsManagerLoaded(adsManagerLoadedEvent) {
   console.log("adsmanagerloaded")
  // Get the ads manager.
  this.adsManager = adsManagerLoadedEvent.getAdsManager(
      this.videoContent);  // See API reference for contentPlayback

  // Add listeners to the required events.
  this.adsManager.addEventListener(
      google.ima.AdErrorEvent.Type.AD_ERROR,
      this.onAdError.bind(this));
  this.adsManager.addEventListener(
      google.ima.AdEvent.Type.CONTENT_PAUSE_REQUESTED,
      this.onContentPauseRequested.bind(this));
  this.adsManager.addEventListener(
      google.ima.AdEvent.Type.CONTENT_RESUME_REQUESTED,
      this.onContentResumeRequested.bind(this));

  try {
    // Initialize the ads manager. Ad rules playlist will start at this time.
    this.adsManager.init(640, 360, google.ima.ViewMode.NORMAL);
    // Call start to show ads. Single video and overlay ads will
    // start at this time; this call will be ignored for ad rules, as ad rules
    // ads start when the this.adsManager is initialized.
    console.log("starting the ad")
    this.adsManager.start();
  } catch (adError) {
    // An error may be thrown if there was a problem with the VAST response.
    // Play content here, because we won't be getting an ad.
    this.videoContent.play();
    console.log("ad error occured",adError)
  }
}

 onContentPauseRequested() {
  // This function is where you should setup UI for showing ads (e.g.
  // display ad timer countdown, disable seeking, etc.)
  this.videoContent.removeEventListener('ended', this.contentEndedListener.bind(this));
  this.videoContent.pause();
  stateManager.setCurrentState("Ad")
}

 onContentResumeRequested() {
  // This function is where you should ensure that your UI is ready
  // to play content.
  this.videoContent.addEventListener('ended', this.contentEndedListener.bind(this));
  this.videoContent.play();
  stateManager.setCurrentState("content")
}
} export default new Ads;